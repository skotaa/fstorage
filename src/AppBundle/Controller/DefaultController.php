<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Exception\FileUploaderFileExistException;
use AppBundle\Form\FileNameEditType;
use AppBundle\Form\StorageUploadedFileType;
use AppBundle\Model\FileNameEdit;
use AppBundle\Model\StorageUploadedFile;
use AppBundle\Service\CurrentDirectoryPath;
use AppBundle\Service\FileUploader;
use AppBundle\Service\ReaderDirectory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage", defaults={"dir": "", "order": "in"})
     * @Route("/go/{order}/{dir}",
     *     name="go_to_dir",
     *     defaults={"dir": "", "order": "in"},
     *     requirements={"order": "in|out"}
     * )
     */
    public function indexAction(Request $request,
                                ReaderDirectory $readerDirectory,
                                CurrentDirectoryPath $currentDirectoryPath,
                                string $order,
                                ?string $dir)
    {
        try {
            $directory = $currentDirectoryPath->setOrder($order)->getRequestedDirectoryPath($dir);
            $storageUploadedFile = new StorageUploadedFile();
            $form = $this->createForm(StorageUploadedFileType::class, $storageUploadedFile, ['action' => $this->generateUrl('upload')]);
            $errorMsg = null;

            return $this->render('@App/default/index.html.twig', [
                'form' => $form->createView(),
                'directoryInfo' => $readerDirectory->getDirectoryInfo($directory),
                'inDir' => $currentDirectoryPath->isFileDirectoryIsRoot() ? false : true,
                'errorMsg' => $errorMsg,
                'newSort' => ReaderDirectory::NAME_SORT_ASC,
                'curSort' => ReaderDirectory::NAME_SORT_DESC,

            ]);

        } catch (Exception $ex) {
            $errorMsg = 'An error occured, something goes wrong.';
        }

        return $this->render('@App/default/error.html.twig', [
            'errorMsg' => $errorMsg
        ]);
    }

    /**
     * @Route("/sortByName/{sort}", name="sort_by_name", requirements={"sort": "asc|desc"})
     */
    public function sortByNameAction(Request $request,
                                ReaderDirectory $readerDirectory,
                                CurrentDirectoryPath $currentDirectoryPath,
                                string $sort)
    {
        try {
            $directory = $currentDirectoryPath->getRequestedDirectoryPath();
        } catch (Exception $ex) {
            $this->addFlash('error', 'An error occured, something goes wrong.');
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('@App/default/directory_list_tbody.html.twig', [
            'directoryInfo' => $readerDirectory->getDirectoryInfo($directory, $sort),
            'inDir' => $currentDirectoryPath->isFileDirectoryIsRoot() ? false : true,
            ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function uploadAction(Request $request,
                                FileUploader $fileUploader,
                                ReaderDirectory $readerDirectory,
                                CurrentDirectoryPath $currentDirectoryPath)
    {
        try {
            $directory = $currentDirectoryPath->getRequestedDirectoryPath();
            $storageUploadedFile = new StorageUploadedFile();
            $form = $this->createForm(StorageUploadedFileType::class, $storageUploadedFile);
            $form->handleRequest($request);
            $errorMsg = null;

            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $fileUploader->upload($storageUploadedFile->getFile(), $directory);
                    $this->addFlash('success', 'Your file were uploaded!');
                } catch (FileUploaderFileExistException $ex) {
                    $this->addFlash('error', sprintf('Given file %s already exists in this directory.', $storageUploadedFile->getFile()->getClientOriginalName()));
                } catch (Exception $ex) {
                    $this->addFlash('error', 'An error occured, file upload failed.');
                }

                return $this->redirect($this->generateUrl('homepage'));
            }

            return $this->render('@App/default/index.html.twig', [
                'form' => $form->createView(),
                'directoryInfo' => $readerDirectory->getDirectoryInfo($directory),
                'inDir' => empty($dir) ? false : true,
                'errorMsg' => $errorMsg
            ]);

        } catch (Exception $ex) {
            $errorMsg = 'An error occured, something goes wrong. '.$ex->getMessage();
        }

        return $this->render('@App/default/error.html.twig', [
            'errorMsg' => $errorMsg
        ]);
    }

    /**
     * @Route("/download/{fileName}", name="download")
     */
    public function downloadAction(Request $request, CurrentDirectoryPath $currentDirectoryPath,
                                   string $fileName)
    {
        try {
            $filePathname = sprintf('%s/%s', rtrim($currentDirectoryPath->getRequestedDirectoryPath(), '/'), $fileName);
            $file = new File($filePathname, true);

            return $this->file($file);
        } catch (Exception $ex) {
            $errorMsg = 'An error occured, something goes wrong. ';
        }

        return $this->render('@App/default/error.html.twig', [
            'errorMsg' => $errorMsg
        ]);
    }


    /**
     * @Route("/delete/{fileName}", name="delete")
     */
    public function deleteAction(Request $request,
                                 CurrentDirectoryPath $currentDirectoryPath,
                                 Filesystem $filesystem,
                                 string $fileName)
    {
        $errorMsg = '';
        try {
            $filePathname = sprintf('%s/%s', rtrim($currentDirectoryPath->getRequestedDirectoryPath(), '/'), $fileName);
            if (false === $filesystem->exists($filePathname)) {
                $this->addFlash('error', sprintf('Given file %s does not exit.', $fileName));
                return $this->redirect($this->generateUrl('homepage'));
            }

            $filesystem->remove($filePathname);

            $this->addFlash('success', 'Delete succeeded');
            return $this->redirect($this->generateUrl('homepage'));
        } catch (Exception $ex) {
            $errorMsg = 'An error occured, something goes wrong. ';
        }

        return $this->render('@App/default/error.html.twig', [
            'errorMsg' => $errorMsg
        ]);
    }

    /**
     * @Route("/formedit/{fileName}", name="form_edit")
     */
    public function editNameFormAction(Request $request,
                                       CurrentDirectoryPath $currentDirectoryPath,
                                       Filesystem $filesystem,
                                       string $fileName)
    {
        $filePathname = sprintf('%s/%s', rtrim($currentDirectoryPath->getRequestedDirectoryPath(), '/'), $fileName);
        $fileNameEdit = (new FileNameEdit())->setFilename($fileName);

        $form = $this->createForm(FileNameEditType::class, $fileNameEdit, ['action' => $this->generateUrl('form_edit', ['fileName' => $fileName])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if (false === $filesystem->exists($filePathname)) {
                    $this->addFlash('error', sprintf('Given file %s does not exit.', $fileName));
                    return $this->redirect($this->generateUrl('homepage'));
                }

                $filePathnameTarget = sprintf('%s/%s', rtrim($currentDirectoryPath->getRequestedDirectoryPath(), '/'), $fileNameEdit->getFilename());

                $filesystem->rename($filePathname, $filePathnameTarget);

                $this->addFlash('success', 'Edit succeeded');
                return $this->redirect($this->generateUrl('homepage'));
            } catch (Exception $ex) {
                $this->addFlash('error', 'An error occured, cannot edit name.');
            }

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('@App/default/form_edit.html.twig', [
            'formedit' => $form->createView()
        ]);
    }

    /**
     * @Route("/formcreate", name="create_folder")
     */
    public function createFolderFormAction(Request $request,
                                       CurrentDirectoryPath $currentDirectoryPath,
                                       Filesystem $filesystem)
    {
        $filePathname = $currentDirectoryPath->getRequestedDirectoryPath();
        $fileNameEdit = (new FileNameEdit())->setFilename('new_folder');

        $form = $this->createForm(FileNameEditType::class, $fileNameEdit, ['action' => $this->generateUrl('create_folder')]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $filePathname = $filePathname.'/'.$fileNameEdit->getFilename();
                if ($filesystem->exists($filePathname)) {
                    $this->addFlash('error', sprintf('Given folder already %s exits.', $fileNameEdit->getFilename()));
                    return $this->redirect($this->generateUrl('homepage'));
                }

                $filesystem->mkdir($filePathname);

                $this->addFlash('success', 'Folder add succeeded');
                return $this->redirect($this->generateUrl('homepage'));
            } catch (Exception $ex) {
                $this->addFlash('error', 'An error occured, cannot create new folder.');
            }

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('@App/default/form_edit.html.twig', [
            'formedit' => $form->createView()
        ]);
    }
}
