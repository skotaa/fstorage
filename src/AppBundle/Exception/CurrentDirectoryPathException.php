<?php
declare(strict_types=1);

namespace AppBundle\Exception;

use Exception;

class CurrentDirectoryPathException extends Exception
{
}
