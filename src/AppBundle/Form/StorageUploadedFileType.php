<?php
declare(strict_types=1);

namespace AppBundle\Form;

use AppBundle\Model\StorageUploadedFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StorageUploadedFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, ['label' => false])
            ->add('submit', SubmitType::class, [
                'label' => 'Upload',
                'attr' => ['class' => 'btn btn-sm btn-default'],
            ]);

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StorageUploadedFile::class,
        ]);
    }
}
