<?php
declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Factory\DirectoryInfoFactory;
use AppBundle\Factory\FinderFactory;
use InvalidArgumentException;
use Symfony\Component\Finder\Finder;

class ReaderDirectory
{
    public const NAME_SORT_ASC = 'asc';
    public const NAME_SORT_DESC = 'desc';

    /**
     * @param string $directory
     * @param null|string $nameSort
     * @return array
     * @throws InvalidArgumentException
     */
    public function getDirectoryInfo(string $directory, ?string $nameSort = null): array
    {
        $finder = FinderFactory::createFinder($directory);

        $finder->directories();
        $directoryInfo = [];
        foreach ($finder as $file) {
            $directoryInfo[] = DirectoryInfoFactory::createDirectoryInfo($file);
        }

        false === empty($nameSort) && $this->getSortByName($finder, $nameSort);
        $finder->files();
        foreach ($finder as $file) {
            $directoryInfo[] = DirectoryInfoFactory::createDirectoryInfo($file);
        }

        return $directoryInfo;
    }

    /**
     * @param Finder $finder
     * @param string $nameSort
     * @throws InvalidArgumentException
     */
    public function getSortByName(Finder $finder, string $nameSort)
    {
        $this->checkAvailableNameSort($nameSort);

        if ($nameSort === self::NAME_SORT_ASC) {
            $finder->sortByName();
        } elseif ($nameSort === self::NAME_SORT_DESC) {
            $finder->sort(function ($a, $b) { return strcmp($b->getFilename(), $a->getFilename()); });
        }
    }

    /**
     * @param string $nameSort
     * @return bool
     */
    public function isSortAsc(string $nameSort): bool
    {
        return $nameSort === self::NAME_SORT_ASC;
    }

    /**
     * @param string $nameSort
     * @return bool
     */
    public function isSortDesc(string $nameSort): bool
    {
        return $nameSort === self::NAME_SORT_DESC;
    }

    /**
     * @return string[]
     */
    private function getNameSortList(): array
    {
        return [self::NAME_SORT_ASC, self::NAME_SORT_DESC];
    }

    /**
     * @param null|string $nameSort
     * @throws InvalidArgumentException
     */
    private function checkAvailableNameSort(string $nameSort): void
    {
        if (false === in_array($nameSort, $this->getNameSortList())) {
            throw new InvalidArgumentException('Invalid %s argument for sorting value', $nameSort);
        }

        return;
    }
}
