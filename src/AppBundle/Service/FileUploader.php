<?php
declare(strict_types=1);

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Filesystem\Exception\IOException;;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use AppBundle\Exception\FileUploaderFileExistException;

class FileUploader
{
    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * FileUploader constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @return Filesystem
     */
    public function getFilesystem(): Filesystem
    {
        return $this->filesystem;
    }

    /**
     * @param UploadedFile $file
     * @param string $targetDir
     * @return File
     * @throws FileException
     * @throws FileUploaderFileExistException
     */
    public function upload(UploadedFile $file, string $targetDir): File
    {
        if (false === $this->existFileOrDir($targetDir)) {
            $this->createDir($targetDir);
        }

        $filePath = $this->prepareFilePath($file, $targetDir);
        if ($this->existFileOrDir($filePath)) {
            throw new FileUploaderFileExistException(sprintf('Given file %s already exist in this directory', $filePath));
        }

        return $file->move($targetDir, $file->getClientOriginalName());
    }

    /**
     * @param string $dir
     * @return bool
     * @throws IOException
     */
    protected function existFileOrDir(string $dir): bool
    {
        return $this->getFilesystem()->exists($dir);
    }

    /**
     * @param string $dir
     * @param int $mode
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected function createDir(string $dir, int $mode = 0777): void
    {
        $this->getFilesystem()->mkdir($dir, $mode);
    }

    /**
     * @param UploadedFile $file
     * @param string $targetDir
     * @return string
     */
    private function prepareFilePath(UploadedFile $file, string $targetDir): string
    {
        return rtrim($targetDir, '/').'/'.$file->getClientOriginalName();
    }
}
