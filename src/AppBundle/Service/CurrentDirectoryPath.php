<?php
declare(strict_types=1);

namespace AppBundle\Service;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface ;
use Symfony\Component\Filesystem\Exception\IOException;

class CurrentDirectoryPath
{
    public const ORDER_IN = 'in';
    public const ORDER_OUT = 'out';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $order;

    /**
     * CurrentDirectoryPath constructor
     *
     * @param ContainerInterface $container.
     * @param RequestStack $request
     * @param string $order
     */
    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request = $requestStack->getCurrentRequest();
        $this->order = self::ORDER_IN;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     * @return CurrentDirectoryPath
     * @throws InvalidArgumentException
     */
    public function setOrder(string $order): CurrentDirectoryPath
    {
        $this->checkAvailableOrder($order);
        $this->order = $order;
        return $this;
    }

    /**
     * @param null|string $newDir
     * @return string
     */
    public function getRequestedDirectoryPath(?string $newDir = null)
    {
        if (empty($newDir) && $this->isOrderIn()) {
            return $this->getSessionFileDirectory($this->getRequest());
        }

        if ($this->isOrderIn()) {
            $newDirPath =  sprintf('%s/%s', rtrim($this->getSessionFileDirectory($this->getRequest()), '/'), $newDir);
        } elseif ($this->isOrderOut()) {
            $newDirPath = $this->getFileDirecotryInOutOrder($this->getRequest());
        }

        $this->checkExistFileDir($newDirPath);
        $this->setSessionFileDirectory($newDirPath);

        return $newDirPath;
    }

    /**
     * @return bool
     */
    public function isFileDirectoryIsRoot(): bool
    {
        if ($this->getSessionFileDirectory($this->getRequest()) === $this->getRootFileDirectory()) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    private function getFileDirecotryInOutOrder(): string
    {
        if ($this->isFileDirectoryIsRoot()) {
            return $this->getSessionFileDirectory($this->getRequest());
        }

        return dirname($this->getSessionFileDirectory($this->getRequest()));
    }

    /**
     * @param string $newDirPath
     * @return bool
     * @throws IOException
     * @throws InvalidArgumentException
     */
    private function checkExistFileDir(string $newDirPath): void
    {
        if (false === $this->getContainer()->get('filesystem')->exists($newDirPath)) {
            throw new InvalidArgumentException(sprintf('Given dir %s does not exist', $newDirPath));
        }
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    private function getSessionFileDirectory(): string
    {
        return $this->getRequest()->getSession()->get('pathname', $this->getRootFileDirectory());
    }

    /**
     * @param $newPathname
     */
    private function setSessionFileDirectory($newPathname): void
    {
        $this->getRequest()->getSession()->set('pathname', $newPathname);
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    private function getRootFileDirectory(): string
    {
        return $this->getContainer()->getParameter('file_directory');
    }

    /**
     * @return string[]
     */
    private function getOrderList(): array
    {
        return [self::ORDER_IN, self::ORDER_OUT];
    }

    /**
     * @param string $order
     * @throws InvalidArgumentException
     */
    private function checkAvailableOrder(string $order): void
    {
        if (false === in_array($order, $this->getOrderList())) {
            throw new InvalidArgumentException('Invalid %s argument for order value', $order);
        }

        return;
    }

    /**
     * @return bool
     */
    private function isOrderIn(): bool
    {
        return $this->getOrder() === self::ORDER_IN;
    }

    /**
     * @return bool
     */
    private function isOrderOut(): bool
    {
        return $this->getOrder() === self::ORDER_OUT;
    }
}
