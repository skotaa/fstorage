<?php
declare(strict_types=1);

namespace AppBundle\Model;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class DirectoryInfo
{
    /**
     * @Assert\NotBlank(message="Please, enter file name.")
     * @Assert\Length(min=1, max=30)
     *
     * @var string $name
     */
    private $name;

    /**
     * @Assert\NotBlank(message="Please, enter pathname.")
     * @Assert\Length(min=1, max=30)
     *
     * @var string $pathName
     */
    private $pathName;

    /**
     * @Assert\DateTime()
     *
     * @var DateTime $mtime
     */
    private $mtime;

    /**
     * @Assert\NotNull()
     *
     * @var int $size
     */
    private $size;

    /**
     * @Assert\NotNull()
     *
     * @var int $numberFiles
     */
    private $numberFiles;

    /**
     * @Assert\NotNull()
     *
     * @var int $dirItem
     */
    private $dirItem;

    /**
     * @Assert\NotNull()
     *
     * @var bool $dir
     */
    private $isDir;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DirectoryInfo
     */
    public function setName(string $name): DirectoryInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathName(): string
    {
        return $this->pathName;
    }

    /**
     * @param string $pathName
     * @return DirectoryInfo
     */
    public function setPathName(string $pathName): DirectoryInfo
    {
        $this->pathName = $pathName;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getMtime(): DateTime
    {
        return $this->mtime;
    }

    /**
     * @return string
     */
    public function getMtimeAsString(): string
    {
        return $this->mtime->format('Y-m-d H:i:s');
    }

    /**
     * @param DateTime $mtime
     * @return DirectoryInfo
     */
    public function setMtime(DateTime $mtime): DirectoryInfo
    {
        $this->mtime = $mtime;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return DirectoryInfo
     */
    public function setSize(int $size): DirectoryInfo
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberFiles(): int
    {
        return $this->numberFiles;
    }

    /**
     * @param int $numberFiles
     */
    public function setNumberFiles(int $numberFiles): void
    {
        $this->numberFiles = $numberFiles;
    }

    /**
     * @return int
     */
    public function getDirItem(): int
    {
        return $this->dirItem;
    }

    /**
     * @param int $dirItem
     * @return DirectoryInfo
     */
    public function setDirItem(int $dirItem): DirectoryInfo
    {
        $this->dirItem = $dirItem;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDir(): bool
    {
        return $this->isDir;
    }

    /**
     * @param bool $isDir
     * @return DirectoryInfo
     */
    public function setIsDir(bool $isDir): DirectoryInfo
    {
        $this->isDir = $isDir;
        return $this;
    }
}
