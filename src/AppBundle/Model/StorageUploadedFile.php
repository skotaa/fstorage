<?php
declare(strict_types=1);

namespace AppBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class StorageUploadedFile
{
    /**
     * @Assert\NotBlank(message="Please, upload the file.")
     * @Assert\File(maxSize = "10M")
     *
     * @var UploadedFile
     */
    private $file;

    /**
     * @return null|UploadedFile
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return StorageUploadedFile
     */
    public function setFile(UploadedFile $file): StorageUploadedFile
    {
        $this->file = $file;
        return $this;
    }
}
