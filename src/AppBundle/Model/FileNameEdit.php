<?php
declare(strict_types=1);

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

class FileNameEdit
{
    /**
     * @Assert\NotBlank(message="Please, enter file name.")
     * @Assert\Length(min=1, max=30)
     *
     * @var string
     */
    private $filename;

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return FileNameEdit
     */
    public function setFilename(string $filename): FileNameEdit
    {
        $this->filename = $filename;
        return $this;
    }
}