<?php
declare(strict_types=1);

namespace AppBundle\Factory;

use Symfony\Component\Finder\Finder;

class FinderFactory
{
    /**
     * @param string $directory
     * @param int $depth
     * @return Finder
     */
    public static function createFinder(string $directory, int $depth = 0): Finder
    {
        return Finder::create()->followLinks()->in($directory)->depth($depth);
    }
}
