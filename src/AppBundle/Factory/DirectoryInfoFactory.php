<?php
declare(strict_types=1);

namespace AppBundle\Factory;

use AppBundle\Model\DirectoryInfo;
use SplFileInfo;
use DateTime;
use InvalidArgumentException;
use Symfony\Component\Finder\Finder;

class DirectoryInfoFactory
{
    /**
     * @param SplFileInfo $file
     * @return DirectoryInfo
     */
    public static function createDirectoryInfo(SplFileInfo $file): DirectoryInfo
    {
        $directoryInfo = new DirectoryInfo();
        $directoryInfo->setIsDir($file->isDir());
        $directoryInfo->setDirItem(0);
        $directoryInfo->setMtime((new DateTime())->setTimestamp($file->getMTime()));
        $directoryInfo->setName($file->getFilename());
        $directoryInfo->setPathName($file->getPathname());
        $directoryInfo->setSize($file->getSize());
        $directoryInfo->setNumberFiles(self::getNumberOfFiles($file));

        return $directoryInfo;
    }

    /**
     * @param SplFileInfo $file
     * @return int
     */
    protected static function getNumberOfFiles(SplFileInfo $fileInfo): int
    {
        $noOfFile = 0;
        if (false === $fileInfo->isDir()) {
            return $noOfFile;
        }

        $finder = (new Finder())->in($fileInfo->getPathName());

        /** @var \SplFileInfo $file */
        foreach ($finder as $file) {
            if (true === $file->isDir()) {
                continue;
            }
            $noOfFile++;
        }

        return $noOfFile;
    }
}
