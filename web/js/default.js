(function($) {
    var $container = $('#container-index');

    $container.find('.btn-edit').on({
        click: function(event) {
            event.preventDefault();
            var btn = $(this).closest('tr[class=file-row]').find('div[class=file-name]');

            $.ajax({
                url: $(this).attr('href'),
                data: {},
                type: 'GET',

                success: function(data){
                    btn.replaceWith('<div class="file-name">'+data+'</div>');
                }
        });
    }});

    $container.find('.btn-folder').on({
        click: function(event) {
            event.preventDefault();
            var div = $(this).closest('div[class=folder-name]');
            $.ajax({
                url: $(this).attr('href'),
                data: {},
                type: 'GET',

                success: function(data){
                    div.replaceWith('<div class="folder-name">'+data+'</div>');
                }
            });
        }});

    $container.find('.btn-sort-name').on({
        click: function(event) {
            event.preventDefault();
            var currentElement = $(this);
            var div = $(this).closest('div').find('tbody[class=directory-row]');
            var sort = $(this).attr('data-sort');
            var sortNext = $(this).attr('data-sort-next');
            var url = $(this).attr('href');

            var urlNew = url.substring(0, url.lastIndexOf('/'))+'/'+sortNext;
            $.ajax({
                url: url,
                data: {},
                type: 'GET',

                success: function(data){
                    currentElement.attr("data-sort", sortNext);
                    currentElement.attr("data-sort-next", sort);
                    currentElement.attr("href", urlNew);
                    currentElement.attr("title", "Sort "+sortNext);
                    div.replaceWith('<tbody class="directory-row">'+data+'</tbody>');
                }
            });
        }});
})(jQuery);