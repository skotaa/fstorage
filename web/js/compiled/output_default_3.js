(function($) {
    var $container = $('#container-index');

    $container.find('.btn-edit').on({
        click: function(event) {
            event.preventDefault();
            var btn = $(this).closest('tr[class=file-row]').find('div[class=file-name]');

            $.ajax({
                url: $(this).attr('href'),
                data: {},
                type: 'GET',

                success: function(data){
                    btn.replaceWith('<div class="file-name">'+data+'</div>');
                }
        });
    }});

    $container.find('.btn-folder').on({
        click: function(event) {
            event.preventDefault();
            var div = $(this).closest('div[class=folder-name]');
            $.ajax({
                url: $(this).attr('href'),
                data: {},
                type: 'GET',

                success: function(data){
                    div.replaceWith('<div class="folder-name">'+data+'</div>');
                }
            });
        }});

    $container.find('.btn-sort-name').on({
        click: function(event) {
            event.preventDefault();
            var div = $(this).closest('div[class=table-directory]');
console.log(div);
            $(this).replaceWith('<div class="table-directory table-responsive">cc</div>');
            // $.ajax({
            //     url: $(this).attr('href'),
            //     data: {},
            //     type: 'GET',
            //
            //     success: function(data){
            //         div.replaceWith('<div class="table-directory table-responsive">'+data+'</div>');
            //     }
            // });
        }});
})(jQuery);